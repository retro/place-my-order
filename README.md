# Installation

 - Checkout the code
 - Checkout `git@bitbucket.org:retro/keechma.git` to `client/checkouts/keechma` (http://jakemccrary.com/blog/2012/03/28/working-on-multiple-clojure-projects-at-once/)
 -  run `lein figwheel dev` inside the `client` folder
 -  run `npm start` inside the `server` code
 -  go to `locahost:3030`